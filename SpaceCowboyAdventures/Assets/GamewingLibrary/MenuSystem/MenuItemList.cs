﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using GamewingResourceLibrary;

namespace GamewingResourceLibrary.MenuSystem {

	public class MenuItemList: MonoBehaviour {
		public bool enabled = true;
		public List<MenuItem> menuItemList;
		int listIndex = 0;
		protected Vector3 targetPosition;
		MenuItemList ParentMenu;
		public float ItemSize = 64f;
		public float ItemSpacing = 64f;

		public SuperInput.ControllerType controller = null;
		public static M Create<M> (Vector3 pos) where M : MenuItemList{
			GameObject go = new GameObject ("Generic Menu");
			M l = go.AddComponent<M> ();
			//l.transform.position = new Vector3(-512, 32);
			l.targetPosition = pos;
			l.menuItemList = new List<MenuItem> ();
			l.SetFocus ();
			l.Start ();

			return l;
		}

		void Init(){
			for (int i = 0; i < menuItemList.Count; i++) {
				var m = menuItemList [i];
				if (listIndex == i) {
					m.font.StringColor = m.colorSelected;
				} else {
					m.font.StringColor = m.colorDeselected;
				}
			}
		}

		void Awake () {
			controller = GlobalInput.Instance.controller;
		}

		virtual public void Start(){

			Init ();
			transform.localScale = new Vector3 (0.01f, 1, 1);
			Animate ();
		}

		public void Animate(){
			//StartCoroutine (Co_Slide (new Vector3 (-512, 32), targetPosition, 0.5f));
			StartCoroutine(Co_Grow(Vector3.one, 0.5f));
		}

		public void SetSubList (MenuItemList newMenu) {
			newMenu.SetFocus ();
			newMenu.ParentMenu = this;
			newMenu.transform.SetParent(transform, false);
			UnsetFocus ();
		}

		void UnsetFocus(){
			enabled = false;
			foreach (MenuItem m in menuItemList) {
				m.FocusLost ();
			}
		}

		void SetFocus(){
			enabled = true;
			foreach (MenuItem m in menuItemList) {
				m.FocusGained ();
			}
		}

		public void DestroyMenuChain(){
			MenuItemList currentMenu = this;
			do{
				currentMenu.DisposeOfMenu();
				currentMenu = currentMenu.ParentMenu;
			}while(currentMenu != null);
		}

		public void AddItem (MenuItem item) {
			menuItemList.Add (item);
			item.gameObject.transform.SetParent (gameObject.transform);
			item.Size = ItemSize;
			item.transform.localScale = Vector3.one;
			float offset = menuItemList.Count * ItemSpacing;
			item.SetPosition (new Vector3 (0, ItemSpacing - offset, 0));
		}

		public bool RemoveItem (MenuItem item) {
			return menuItemList.Remove (item);
		}

		int MoveUp () {
			menuItemList [listIndex].Deselected ();
			menuItemList [listIndex].selected = false;
			listIndex--;
			if (listIndex < 0) {
				listIndex += menuItemList.Count;
			}
			menuItemList [listIndex].Selected ();
			menuItemList [listIndex].selected = true;
			return listIndex;
		}

		int MoveDown () {
			menuItemList [listIndex].Deselected ();
			menuItemList [listIndex].selected = false;
			listIndex++;
			if (listIndex >= menuItemList.Count) {
				listIndex -= menuItemList.Count;
			}
			menuItemList [listIndex].Selected ();
			menuItemList [listIndex].selected = true;
			return listIndex;
		}

		public MenuItem GetSelected () {
			return menuItemList [listIndex];
		}

		void Update () {
			if (!enabled) {
				return;
			}

			if (controller.getPressedNegative (1)) {
				MoveUp ();
			}
			if (controller.getPressedPositive (1)) {
				MoveDown ();
			}
			if (controller.getPressed (2)) {
				GetSelected ().Action ();
			}
		}

		void OnGUI () {
			Vector3 pos = new Vector3 (32, 32, 0);
			Rect rec = new Rect (0, 0, 100, 20);
			for (int i = 0; i < menuItemList.Count; i++) {
				menuItemList [i].DrawItem (rec);
				pos += new Vector3 (0, 32, 0);
			}
		}

		protected virtual void DisposeOfMenu () {
			StartCoroutine (Co_LeaveScreen (transform.position, new Vector3 (-512, 32, 0)));
		}

		IEnumerator Co_Slide (Vector3 pos1, Vector3 pos2, float duration) {
			float timer = 0f;

			while (timer < duration) {
				transform.position = Vector3.Lerp (pos1, pos2, timer / duration);
				timer += Time.unscaledDeltaTime;
				yield return null;
			}

			if(menuItemList[listIndex] != null){
				menuItemList [listIndex].Selected ();
			}

			transform.position = pos2;
		}

		IEnumerator Co_Grow(Vector3 targetScale, float duration){
			float timer = 0f;
			Vector3 startScale = transform.localScale;

			while (timer < duration) {
				transform.localScale = Vector3.Lerp (startScale, targetScale, timer / duration);
				timer += Time.unscaledDeltaTime;
				yield return null;
			}
			transform.localScale = targetScale;

			if(menuItemList[listIndex] != null){
				menuItemList [listIndex].Selected ();
			}

			Init ();
		}

		IEnumerator Co_LeaveScreen (Vector3 pos1, Vector3 pos2) {
			enabled = false;
			//yield return Co_Slide (transform.position, new Vector3 (-512, 32), 0.5f);
			yield return Co_Grow(new Vector3(0,1,1), 0.5f);

			if (ParentMenu != null) {
				ParentMenu.SetFocus ();
			}

			Destroy (gameObject);
		}
	}
		
}

