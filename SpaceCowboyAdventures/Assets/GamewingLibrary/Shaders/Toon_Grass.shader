Shader "Toon/Grass" {
         Properties {
             _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
             _MainTex ("Base (RGB)", 2D) = "white" {}
             _Normal ("Normal Map", 2D) = "bump" {}
             _Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
             _MotionMap ("Motion Map (RGB)", 2D) = "gray" {}
             _MotionScale ("Motion Scale", float) = 1.0
             _AnimSpeed ("Animation Speed", float) = 1.0
             _Tolerance ("Clip Tolerance", Range(0.0,1.0)) = 0.5
         }
     
         SubShader {
             Tags {"RenderType"="Transparent" "Queue"="Geometry"}
             zwrite on
             Blend SrcAlpha OneMinusSrcAlpha
             LOD 200
             
     CGPROGRAM
     #pragma surface surf ToonRamp vertex:vert ToonRamp addshadow
     
     sampler2D _Ramp;
     float _Tolerance;

     // custom lighting function that uses a texture ramp based
     // on angle between light direction and normal
     #pragma lighting ToonRamp exclude_path:prepass
     inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
     {
         #ifndef USING_DIRECTIONAL_LIGHT
         lightDir = normalize(lightDir);
         #endif
         
         half d = dot (s.Normal, lightDir)*0.5 + 0.5;
         half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
         
         half4 c;
         c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
         c.a = s.Alpha;
         //clip(s.Alpha - _Tolerance);
         return c;
     }
     
     
     sampler2D _MainTex;
     sampler2D _Normal;
     sampler2D _MotionMap;
     float4 _Color;
     float _animSpeed;
     float _MotionScale;

     struct Input {
         float2 uv_MainTex : TEXCOORD0;
         float2 uv_Normal : TEXCOORD1;
         float2 uv_Motion : TEXCOORD2;
     };

     void vert(inout appdata_full v){
     	//displacement
     	//float x = tex2Dlod(_MotionMap, float4(v.texcoord1.xy + float2(_animSpeed * _Time.x, 0),0,0)).r;
     	float x = tex2Dlod(_MotionMap, float4(v.texcoord2 + float2(_animSpeed * _Time.x, 0),1,1)).r;
     	//v.vertex.x += x * _MotionScale;
     	v.vertex.x += v.normal * _MotionScale;
     }

     void surf (Input IN, inout SurfaceOutput o) {
         half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
         o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_Normal));
         o.Albedo = c.rgb;
         o.Alpha = c.a;
         clip(c.a - _Tolerance);
     }
     ENDCG
     
         } 
     
         Fallback "Transparent/Cutout"
     }