Shader "Toon/Advanced_Transparent" {
         Properties {
         	[KeywordEnum (Off, Front, Back)] _CullMode ("Cull", int) = 2
            _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _Shininess ("Specular", range(0.0,1.0)) = 0.0
            _Normal ("Normal Map", 2D) = "bump" {}
            _Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
            _CutoffTex ("Cutoff Image (A)", 2D) = "white" {}
            _Cutoff ("Alpha Cutoff", Range(0.0,1.0)) = 0.5
            _GlowTex ("Glow Texture", 2D) = "white" {}
			_GlowIntensity ("Glow Intensity", float) = 0.0
			[KeywordEnum (Off, Blend, Multiply, ProbeBlend, ProbeMultiply)] _ReflMode ("Reflection Mode", int) = 0
			_Cube ("Reflection", Cube) = "_SkyBox" {}
			_ReflIntensity ("Reflection Intensity", range(0.0,1.0)) = 0.5
         }

         SubShader {
             Tags {"RenderType"="Transparent" "Queue"="Transparent"}
             //usepass "Legacy/Specular/FORWARD"
             Blend SrcAlpha OneMinusSrcAlpha
             cull [_CullMode]
             Zwrite on
     		 
             LOD 600

             
     CGPROGRAM
     #pragma surface surf ToonRamp fullforwardshadows keepalpha addshadow
     
     sampler2D _Ramp;
     float _Cutoff;
     sampler2D _GlowTex;
	 int _UseGlowTex;
	 float _GlowIntensity;
	 int _ReflMode;
	 float _ReflIntensity;
     
     // custom lighting function that uses a texture ramp based
     // on angle between light direction and normal
     #pragma lighting ToonRamp exclude_path:prepass
     inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
     {
         #ifndef USING_DIRECTIONAL_LIGHT
         lightDir = normalize(lightDir);
         #endif
         
         half d = dot (s.Normal, lightDir)*0.5 + 0.5;
         half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
         
         half4 c;
         c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
         c.a = s.Alpha;
         //clip(s.Alpha - _Cutoff);
         return c;
     }
     
     
     sampler2D _MainTex;
     sampler2D _CutoffTex;
     sampler2D _Normal;
     float4 _Color;
     samplerCUBE _Cube;



     struct Input {
         float2 uv_MainTex : TEXCOORD0;
         float2 uv_Normal : TEXCOORD1;
         float2 uv_GlowTex : TEXCOORD2;
         float2 uv_CutoffTex : TEXCOORD3;
         float3 worldRefl;
         INTERNAL_DATA
     };
     
     void surf (Input IN, inout SurfaceOutput o) {
         half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
         half cut = tex2D(_CutoffTex, IN.uv_CutoffTex).a;
         half3 g = tex2D(_GlowTex, IN.uv_GlowTex).rgb;
         o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_Normal));

         float3 worldRefl = WorldReflectionVector (IN, o.Normal);
         fixed4 reflcol;
         if(_ReflMode == 3 || _ReflMode == 4){
         	reflcol = float4(UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl));
         }else{
		 	reflcol = texCUBE (_Cube, worldRefl);
	 	 }
		 //fixed4 reflcol = texCUBE (unity_SpecCube0, worldRefl);

		 clip(min(c.a,cut) - _Cutoff);

         o.Emission = g.rgb * _GlowIntensity;
         if(_ReflMode == 0){
         	o.Albedo = c.rgb;
         }else if(_ReflMode == 1 || _ReflMode == 3){//blend
         	o.Albedo = lerp(c.rgb, reflcol.rgb, _ReflIntensity * reflcol.a) * _Color.rgb;
         }else if(_ReflMode == 2 || _ReflMode == 4){//multiply
         	o.Albedo = c.rgb * lerp(float3(1,1,1), reflcol.rgb, _ReflIntensity) * _Color.rgb;
         }
         o.Alpha = c.a;
     }
     ENDCG
     
         } 
     
         Fallback "Transparent/Diffuse"
     }