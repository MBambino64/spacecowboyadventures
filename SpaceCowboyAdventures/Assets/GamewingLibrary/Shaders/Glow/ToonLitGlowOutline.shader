Shader "Toon/Glow/Lit Outline" {
	Properties {
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.002, 0.03)) = .005
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
		_GlowTex ("Glow Texture", 2D) = "black" {}
		_GlowIntensity ("Glow Intensity", float) = 0.0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		UsePass "Toon/Glow/Lit/FORWARD"
		UsePass "Toon/Basic Outline/OUTLINE"
	} 
	
	Fallback "Toon/Lit"
}
