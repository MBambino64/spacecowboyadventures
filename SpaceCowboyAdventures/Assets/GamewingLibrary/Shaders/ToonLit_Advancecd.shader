Shader "Toon/Advanced" {
         Properties {
         	[KeywordEnum (Off, Front, Back)] _CullMode ("Cull", int) = 2
            _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _Normal ("Normal Map", 2D) = "bump" {}
            _NormalIntens ("Normal Intensity", float) = 1.0
            _Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
            _CutoffTex ("Cutoff Image (A)", 2D) = "white" {}
            _Cutoff ("Alpha Cutoff", Range(0.0,1.0)) = 0.5
            _GlowTex ("Glow Texture", 2D) = "white" {}
			_GlowIntensity ("Glow Intensity", float) = 0.0
			[KeywordEnum (Off, Blend, Multiply, ProbeBlend, ProbeMultiply)] _ReflMode ("Reflection Mode", int) = 0
			_Cube ("Reflection", Cube) = "_SkyBox" {}
			_ReflIntensity ("Reflection Intensity", range(0.0,1.0)) = 0.5
			_ReflMask ("Reflection Mask", 2D) = "white" {}

         }

         SubShader {
             Tags {"RenderType"="Transparent"}
             Blend SrcAlpha OneMinusSrcAlpha
             cull [_CullMode]
             Zwrite On
             LOD 600

             
     CGPROGRAM
     #pragma surface surf ToonRamp fullforwardshadows //alpha
     #include "UnityCG.cginc"

     float _NormalIntens;
     sampler2D _Ramp;
     float _Cutoff;
     sampler2D _GlowTex;
	 int _UseGlowTex;
	 float _GlowIntensity;
	 int _ReflMode;
	 float _ReflIntensity;
	 sampler2D _ReflMask;
     
     // custom lighting function that uses a texture ramp based
     // on angle between light direction and normal
     #pragma lighting ToonRamp exclude_path:prepass
     inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
     {
         #ifndef USING_DIRECTIONAL_LIGHT
         lightDir = normalize(lightDir);
         #endif
         
         half d = dot (s.Normal, lightDir)*0.5 + 0.5;
         half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
         
         half4 c;
         c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
         c.a = s.Alpha;
         //clip(s.Alpha - _Cutoff);
         return c;
     }
     
     
     sampler2D _MainTex;
     sampler2D _CutoffTex;
     sampler2D _Normal;
     float4 _Color;
     samplerCUBE _Cube;



     struct Input {
         float2 uv_MainTex : TEXCOORD0;
         float2 uv_Normal : TEXCOORD1;
         float2 uv_GlowTex : TEXCOORD2;
         float2 uv_CutoffTex : TEXCOORD3;
         float3 worldRefl;
         INTERNAL_DATA
     };
     
     void surf (Input IN, inout SurfaceOutput o) {
         half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
         half cut = tex2D(_CutoffTex, IN.uv_CutoffTex).a;
         half3 g = tex2D(_GlowTex, IN.uv_GlowTex).rgb;
         half3 refl = tex2D(_ReflMask, IN.uv_MainTex).rgb;
         o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));

         float3 worldRefl = WorldReflectionVector (IN, o.Normal);
         fixed4 reflcol;
         if(_ReflMode == 3 || _ReflMode == 4){
         	reflcol = float4(UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl));
         }else{
		 	reflcol = texCUBE (_Cube, worldRefl);
	 	 }
	 	 reflcol *= _Color;
		 //fixed4 reflcol = texCUBE (unity_SpecCube0, worldRefl);

		 clip(min(c.a,cut) - _Cutoff);

         o.Emission = g.rgb * _GlowIntensity;
         if(_ReflMode == 0){
         	o.Albedo = c.rgb;
         }else if(_ReflMode == 1 || _ReflMode == 3){//blend
         	o.Albedo = lerp(c.rgb, reflcol.rgb, _ReflIntensity * reflcol.a * refl.r);
         }else if(_ReflMode == 2 || _ReflMode == 4){//multiply
         	o.Albedo = c.rgb * lerp(float3(1,1,1), reflcol.rgb, _ReflIntensity * refl.r);
         }
         o.Alpha = c.a;
     }
     ENDCG
     
         } 
     
         Fallback "Transparent/Cutout/Diffuse"
     }