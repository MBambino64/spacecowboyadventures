﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawBitmapFont : MonoBehaviour {

	#region editor controls
	[SerializeField]
	/// <summary>
	/// will show the approximate string length in the editor
	/// </summary>
	int previewLength = 10;
	[SerializeField]
	/// <summary>
	/// The default text.
	/// </summary>
	string defaultText;
	#endregion

	public static DrawBitmapFont instance;
	public static Texture2D defaultFont = null;

	public const string Clr_Red = "FF0000";
	public const string Clr_Orange = "FF8000";
	public const string Clr_Yellow = "FFFF00";
	public const string Clr_Green = "008000";
	public const string Clr_Blue = "0000FF";
	public const string Clr_Purple = "800080";
	public const string Clr_Black = "000000";
	public const string Clr_White = "FFFFFF";
	public const string Clr_Cyan = "00FFFF";
	public const string Clr_Magenta = "FF00FF";
	public const string Clr_Pink = "FF4080";
	public const string Clr_Gray = "808080";
	public const string Clr_LtGray = "C0C0C0";
	public const string Clr_DkGray = "404040";
	public const string Clr_Lime = "00FF00";
	public const string Clr_Brown = "804000";

	#region Superstring information
	string m_text; //stored string
	float m_length; //pixel length of the string, calculated on parse
	float m_height; //pixel height of the string, calculated on parse
	Color m_stringColor = Color.black;

	//public
	public Color defaultColor = Color.black;
	public List<SuperChar> m_chars = new List<SuperChar>();
	public float stringSize = 24f;
	public float m_HSpacing = 0.6f;
	public float m_VSpacing = 1.1f;
	public Halign Align_H;
	public Valign Align_V;
	#endregion

	/// <summary>
	/// Gets or sets the text.
	/// Setting the string automatically parses it
	/// </summary>
	/// <value>The text.</value>
	public string Text{
		get{
			return m_text;
		}
		set{
			m_text = value;
			defaultText = value;
			ParseString(value);
		}
	}

	public Color StringColor{
		get{ 
			return m_stringColor;
		}
		set{ 
			m_stringColor = value;
			foreach (SuperChar c in m_chars) {
				c.color = m_stringColor;
			}
		}
	}

	public class charTransform{
		public Vector3 position = Vector3.zero;
		public Vector3 scale = Vector3.one;
		public Quaternion roation = Quaternion.identity;
	}

	[System.Serializable]
	public class SuperChar{
		public char Character;
		public Color color;
		public charTransform transform;
		public Texture2D font;
		public Rect charSize;
		public Rect textureSlice;
		//public SuperCharAction[] actionList;
		public int actionMask;
		//Formatting Commands		codes can be combined with commas eg. <~,sh,sv,s2.0>
		//Bitmask	name			code		description
		//--------------------------------------------------------------------------------------
		//							<cHEXCOL>	sets character color
		//							<fxFLOAT>	sets effect speed
		//							<sFLOAT>	sets character scale
		//							<n>			starts new line
		//							<t>			inserts tab
		//							<chr0-255>	inserts ascii character
		//							<L>			toggles format lock (should further formatting be read excluding this?)
		//							<F0-7>		changes font (if a font doesn't exist, the default will be used)
		//0x0000	default			<~>			resets formatting (including format lock)
		//0x0001	shake_h			<sh>		shakes characters side-to-side
		//0x0002	shake_v			<sv>		shakes characters up and down
		//0x0004	swivel			<sw>		see-saws characters
		//0x0008	rotate_cw		<r+>		constantly rotates characters clockwise
		//0x0010	rotate_ccw		<r->		constantly rotates characters couter-clockwise
		//0x0020	fade			<f>			fades characters in and out
		//0x0040	fade_color		<fHEXCOL>	fades characters between color and color2
		public SuperChar(char chr, charTransform m_transform, Color col, Texture2D fontImg, int effectMask = 0x0000){
			Character = chr;
			color = col;
			transform = m_transform;
			font = fontImg;
			actionMask = effectMask;
		}
	}

	public int Bitmask_AND(int current, int mask){
		return current & mask;
	}
	public int Bitmask_XOR(int current, int mask){
		return current & mask;
	}

	public enum Valign
	{
		Top, Middle, Bottom
	};

	public enum Halign
	{
		Left, Center, Right
	};

	public List<Texture2D> Fonts = new List<Texture2D>();

	public char linebreak = '`';

	Rect[] charpos = new Rect[256];

	#region MonoBehavior Methods
	void Start(){
		//load default font
		if(Fonts.Count == 0){
			Fonts.Add (defaultFont);
		}else if(Fonts[0] == null){
			Fonts[0] = defaultFont;
		}

		//set string
		Text = defaultText;
	}

	void Awake(){
		if (instance == null) {
			instance = this;
		}

		if (defaultFont == null) {
			defaultFont = Resources.Load <Texture2D> ("Fonts/Impact_Outline");
		}

		if(Fonts.Count == 0){
			Fonts.Add (defaultFont);
		}else if(Fonts[0] == null){
			Fonts[0] = defaultFont;
		}

		//init character rects
		int c = 0;
		for (float cy = 0; cy < 16; cy++) {
			for(float cx = 0; cx < 16; cx++){
				charpos[c] = new Rect(cx / 16f, (15f / 16f) - cy / 16f, 1f / 16f, 1f / 16f);
				c++;
			}
		}
	}

	void Update(){
		#region DEBUG
		if (Input.GetKeyDown (KeyCode.Alpha0)) {
			//ParseString ("<Command> Hello,<Com2><~>");
		}
		if (Input.GetKeyDown (KeyCode.Delete)) {
			if(defaultText.Trim() != ""){
				Text = defaultText;
			}else{
				Text = Text + "Test string\n";
			}
			//output = int.Parse (testHex,System.Globalization.NumberStyles.HexNumber);
		}
		#endregion
	}

	void OnGUI(){
		Quaternion q = Quaternion.Euler (transform.eulerAngles.x,
			transform.eulerAngles.y, -transform.eulerAngles.z);
		GUI.matrix = Matrix4x4.TRS(new Vector3(transform.position.x,Screen.height - transform.position.y, transform.position.z), q, transform.lossyScale);

		Vector2 localCharPos = Vector2.zero;
		Vector2 localOffset = Vector2.zero;

		//get alignment
		switch (Align_H){
		case Halign.Left:
			localOffset.x = 0;
			break;
		case Halign.Center:
			localOffset.x = StringLength / 2f;
			break;
		case Halign.Right:
			localOffset.x = StringLength;
			break;
		}

		switch (Align_V){
		case Valign.Top:
			localOffset.y = 0;
			break;
		case Valign.Middle:
			localOffset.y = StringHeight / 2f;
			break;
		case Valign.Bottom:
			localOffset.y = StringHeight;
			break;
		}

		localCharPos = -localOffset;

		for (int i = 0; i < m_chars.Count; i++){
			SuperChar c = m_chars[i];
			if(c.Character == '\n'){
				//reset line position
				localCharPos.x = localOffset.x;
				localCharPos.y += stringSize * m_VSpacing;
			}else{
				GUI.color = c.color;
				if (c.font != null) {
					GUI.DrawTextureWithTexCoords (
						new Rect (localCharPos, c.charSize.max), c.font, c.textureSlice
					);
				}

				localCharPos.x += c.charSize.width * m_HSpacing;
			}
		}

		GUI.matrix = Matrix4x4.identity;
	}

	void OnDrawGizmos(){
		//OnGUI();
		try{
			Gizmos.matrix = Matrix4x4.TRS(new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation, transform.lossyScale);
		}catch (System.Exception e){

		}

		Vector2 localCharPos = Vector2.zero;
		Vector2 localOffset = Vector2.zero;
		float size = (previewLength+1) * stringSize * m_HSpacing;

		//get alignment
		switch (Align_H){
		case Halign.Left:
			localOffset.x = 0;
			break;
		case Halign.Center:
			localOffset.x = size / 2f;
			break;
		case Halign.Right:
			localOffset.x = size;
			break;
		}

		switch (Align_V){
		case Valign.Top:
			localOffset.y = 0;
			break;
		case Valign.Middle:
			localOffset.y = size / 2f;
			break;
		case Valign.Bottom:
			localOffset.y = size;
			break;
		}

		localCharPos -= localOffset;

		for (int i = 0; i < previewLength; i++) {
			//localCharPos.x = localOffset.x;
			//localCharPos.y += stringSize * m_VSpacing;
			Gizmos.color = defaultColor;
			//Gizmos.DrawLine(new Vector3(0,stringSize,0),new Vector3(40,stringSize,0));
			Gizmos.DrawWireCube (new Vector3 (localCharPos.x + stringSize / 2f, localCharPos.y - stringSize / 2f, -0.25f), new Vector3 (stringSize, stringSize, 0));

			localCharPos.x += stringSize * m_HSpacing;
		}

		Gizmos.matrix = Matrix4x4.identity;
	}
	#endregion

	#region generators
	public static DrawBitmapFont Create(){
		GameObject go = new GameObject ("Text");
		DrawBitmapFont f = go.AddComponent<DrawBitmapFont> ();

		return f;
	}
	public static DrawBitmapFont Create(string text, float size){
		GameObject go = new GameObject (text);
		DrawBitmapFont f = go.AddComponent<DrawBitmapFont> ();
		f.stringSize = size;
		f.Text = text;

		return f;
	}
	#endregion

	public Color32 ToColor(int HexVal)
	{
		byte R = (byte)((HexVal >> 16) & 0xFF);
		byte G = (byte)((HexVal >> 8) & 0xFF);
		byte B = (byte)((HexVal) & 0xFF);
		return new Color32(R, G, B, 255);
	}

	public void ParseString (string str){
		Color color = defaultColor;

		m_chars.Clear();
		char[] chars = str.ToCharArray();

		//print(str);
		if (chars.Length == 0) {
			return;
		}

		foreach (char c in chars){
			charTransform t = new charTransform();
			SuperChar super = new SuperChar(c, t, color, Fonts[0]);
			super.textureSlice = charpos[(int)c];
			super.charSize = new Rect(0,0,stringSize,stringSize);
			m_chars.Add(super);
		}

		m_length = m_chars[0].charSize.width * m_HSpacing;
		foreach(SuperChar s in m_chars){
			m_length += s.charSize.width * m_HSpacing;
		}
	}

	public float StringLength{
		get{ 
			return m_length;
		}
	}

	public float StringHeight{
		get{ 
			return stringSize;
		}
	}

	/*
	SuperString ParseString(string str){
		char[] text = str.ToCharArray ();
		SuperString superStr = new SuperString ();;
		List<SuperChar> charList = new List<SuperChar>();

		bool LockFormat = false;
		Vector2 offset = Vector2.zero;
		Vector2 scale = Vector2.one;
		Color textCol = Color.white;
		Texture2D currentFont = Fonts [0];
		int bitmask = 0x0000;

		int index = 0; //current character position
		int indexNext = 0; //position of the next '>'
		int indexSub = 0; //position of the next ','
		char currentChar;
		string command;

		while(index < text.Length){
			currentChar = text[index];
			if(LockFormat){//check for unlock
				if(str.Substring(index, 3) == "<L>"){
					index += 3;
					LockFormat = false;
				}
			}

			if(!LockFormat && currentChar == '<' && str.IndexOf ('>', index) > 0){
				indexNext = str.IndexOf ('>', index);
				while (index < indexNext) {
					//get commands
					if (str.IndexOf (',', index, indexNext - index) != -1) {
						indexSub = str.IndexOf (',', index, indexNext - index);
						command = str.Substring (index+1, indexSub - index - 1);
						index = indexSub;
					} else {
						command = str.Substring (index+1, indexNext - index - 1);
						index = indexNext;
					}
					print (command);

					//parse commands
					if(command == "L"){//lock format
						LockFormat = true;
					}else if(command == "~"){//reset to default
						bitmask = 0x0000;
						scale = Vector2.one;
						textCol = Color.white;
						currentFont = Fonts [0];
					}else if(command == "sh"){
						bitmask = Bitmask_XOR(bitmask, 0x0001);
					}else if(command == "sv"){
						bitmask = Bitmask_XOR(bitmask, 0x0002);
					}
				}
			}else{
				SuperChar tempChar = new SuperChar(text[index], offset, textCol, currentFont, bitmask);
				charList.Add(tempChar);
			}

			index++;
		}

		superStr.chars = charList.ToArray();
		return superStr;
	}
	*/

	/// <summary>
	/// Draws the string String with advanced formatting
	/// </summary>
	/// <returns>Width of the furthest point in the string.</returns>
	/// <param name="position">Position on screen.</param>
	/// <param name="spacing">Space between characters.</param>
	/// <param name="size">Font size.</param>
	/// <param name="String">Text.</param>
	/// <param name="H">Horizontal Alignment.</param>
	/// <param name="V">Vertical Alignment.</param>
	/// <param name="Font">OPTIONAL: change the font used
	public float DrawFont(Vector2 position, float spacing, float size, string String, Halign H, Valign V, Texture Font = null){

		int length = String.Length;
		char[] chars = String.ToCharArray ();

		float effectSpeed = 1;
		bool useShake = false;
		float shakeVal = (Time.time % 4) * 360;
		bool useFade = false;
		float fadeval = (Time.time % 4) * 360;
		Color fadeColor = Color.clear;
		bool useSwivel = false;
		bool useRotate = false;

		int line = 0;
		float pos = 0;
		float lineLength = 0;
		Rect stringpos;
		float sizeScale = 1;

		float maxpos = 0;

		if (Font == null) {
			Font = Fonts[0];
		}

		//draw text
		sizeScale = 1;
		for (int CharInd = 0; CharInd < chars.Length; CharInd++) {
			lineLength = 0f;
			for (int i = CharInd; i < chars.Length; i++) {
				if (chars [i] == '`') {
					break;
				} else if (chars [i] == '|') {
					i += 6;
				} else if (chars [i] == '<' && (String.IndexOf ('>', i, Mathf.Min (6, chars.Length - i)) != -1)) {
					int nextchar = String.IndexOf ('>', i, Mathf.Min (6, chars.Length - i));
					sizeScale = float.Parse (String.Substring (i + 1, nextchar - i - 1));
					i += nextchar - i;
				} else if (chars [i] == '~' && i < chars.Length) {//~ commands
					if (chars [i + 1] == 's') {
						i += 1;
					} else if (chars [i + 1] == 'f') {
						i += 1;
						if (chars [i + 1] == 'c') {//color change
							i += 7;
						}
					} else if (chars [i + 1] == 'r') {
						i += 1;
					} else if (chars [i + 1] == 'R') {
						i += 1;
					}
				} else if (chars [i] == '{' && (String.IndexOf ('}', i, Mathf.Min (6, chars.Length - i)) != -1)) {
					int nextchar = String.IndexOf ('}', i, Mathf.Min (6, chars.Length - i));
					i += nextchar - i;
				} else {
					lineLength += spacing * sizeScale;
				}
			}

			for (int count = CharInd; count < chars.Length; count++) {
				//get linelength

				shakeVal += 60;
				if (chars [count] == '`') {
					line++;
					pos = 0;
					break;
				} else if (chars [count] == '|') {//color change
					string hex = String.Substring (count + 1, 6);
					hex.ToLower ();
					GUI.color = ToColor (int.Parse (hex, System.Globalization.NumberStyles.HexNumber));
					count += 6;
				} else if (chars [count] == '<' && (String.IndexOf ('>', count, Mathf.Min (6, chars.Length - count)) != -1)) {
					int nextchar = String.IndexOf ('>', count, Mathf.Min (6, chars.Length - count));
					sizeScale = float.Parse (String.Substring (count + 1, nextchar - count - 1));
					count += nextchar - count;
				} else if (chars [count] == '~' && count < chars.Length) {//~ commands
					if (chars [count + 1] == 's') {
						useShake = !useShake;
						count += 1;
					} else if (chars [count + 1] == 'f') {
						useFade = !useFade;
						count += 1;
						if (chars [count + 1] == 'c') {//color change
							count++;
							string fc = String.Substring (count + 1, 6);
							fc.ToLower ();
							fadeColor = ToColor (int.Parse (fc, System.Globalization.NumberStyles.HexNumber));
							count += 6;
						}
					} else if (chars [count + 1] == 'r') {
						useSwivel = !useSwivel;
						count += 1;
					} else if (chars [count + 1] == 'R') {
						useRotate = !useRotate;
						count += 1;
					}
				} else if (chars [count] == '{' && (String.IndexOf ('}', count, Mathf.Min (6, chars.Length - count)) != -1)) {
					int nextchar = String.IndexOf ('}', count, Mathf.Min (6, chars.Length - count));
					effectSpeed = float.Parse (String.Substring (count + 1, nextchar - count - 1));
					count += nextchar - count;
				} else {

					CharInd = count;
					float posX = 0;
					float posY = 0;
					//Verticle alignment
					if (V == Valign.Top) {
						posY = 0;
					} else if (V == Valign.Middle) {
						posY = size / 2f;
					} else {
						posY = size;
					}
					//Horizontal Alignmet
					if (H == Halign.Left) {
						posX = 0;
					} else if (H == Halign.Center) {
						posX = lineLength / 2f;
					} else {
						posX = lineLength;
					}

					if (useShake) {
						stringpos = new Rect (position.x + pos - posX, position.y + Mathf.Sin (shakeVal * effectSpeed * Mathf.Deg2Rad) * size / 4 + (float)line * spacing * sizeScale - posY, size * sizeScale, size * sizeScale);
					} else {
						stringpos = new Rect (position.x + pos - posX, position.y + (float)line * spacing * sizeScale - posY, size * sizeScale, size * sizeScale);
					}

					if (count > String.Length) {
						count = String.Length;
					}

					Color prevcolor = GUI.color;
					if (useFade) {
						GUI.color = Color.Lerp (GUI.color, fadeColor, Mathf.Sin (fadeval * effectSpeed * Mathf.Deg2Rad));
					}

					GUI.matrix = Matrix4x4.identity;

					if (useSwivel) {
						GUIUtility.RotateAroundPivot (Mathf.Sin (shakeVal * effectSpeed * Mathf.Deg2Rad) * 30, stringpos.center);
					}

					if (useRotate) {
						GUIUtility.RotateAroundPivot (shakeVal, stringpos.center);
					}

					GUI.DrawTextureWithTexCoords (stringpos, Font, charpos [(int)chars [count]]);
					pos += spacing * sizeScale;

					GUI.color = prevcolor;


					if (pos >= maxpos) {
						maxpos = pos;
					}
				}
			}
		}
		return maxpos;
	}
	/// <summary>
	/// Draws the string String with basic formatting
	/// </summary>
	/// <returns>Width of the furthest point in the string.</returns>
	/// <param name="position">Position on Screen.</param>
	/// <param name="size">Font size.</param>
	/// <param name="String">Text.</param>
	public float DrawFont(Vector2 position, float size, string String, Halign H = Halign.Left, Valign V = Valign.Top){
		return DrawFont (position, size * 0.75f, size, String, H, V);
	}

}
