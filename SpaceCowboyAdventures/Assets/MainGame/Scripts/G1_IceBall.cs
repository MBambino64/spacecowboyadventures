﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_IceBall : MonoBehaviour {

	public float BreakForce = 0f;
	public float Distance = 20f;

	Rigidbody physics;

	void Start(){
		physics = GetComponent<Rigidbody>();
	}

	void Break(){
		Vector2 screenSpace = new Vector2();

		foreach(var c in GameObject.FindObjectsOfType<Game1Camera>()){
			float dis = Vector3.Distance (c.transform.position, transform.position);
			if (dis <= Distance) {
				Vector3 screen = c.Camera.WorldToViewportPoint (transform.position);
				screenSpace.x = screen.x;
				screenSpace.y = screen.y;

				c.RadialExplosion (0.5f, (1 - (dis / Distance)) * 0.5f, screenSpace);
			}
		}

		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision col){
		if(col.impulse.magnitude > BreakForce){
			G1_CharacterBase chr = col.gameObject.GetComponent<G1_CharacterBase>();
			if(chr != null){
				chr.ApplyDamage(1, G1_CharacterBase.DamageType.Crushed);
			}
			Break();
		}
	}
}
