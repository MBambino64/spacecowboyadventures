﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_SpikeFloor : MonoBehaviour
{
    public int Damage = 1000;

    void OnTriggerEnter(Collider col)
    {
        G1_CharacterBase c = col.gameObject.GetComponent<G1_CharacterBase>();
        if (c == null)
        {
            return;
        }

        c.ApplyDamage(Damage, G1_CharacterBase.DamageType.Damage);
    }
}
