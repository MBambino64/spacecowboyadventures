﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G1_CollectibleItemDisplay : MonoBehaviour {

	public RawImage Img_Icon;
	public DrawBitmapFont Txt_Count;

	public G1_CollectibleSystem system;
	public Texture2D image;

	void Start(){
		//setImageComponent
		if(image != null){
			Img_Icon.texture = image;
		}

		if(system.Icon != null){
			Img_Icon.texture = system.Icon;
		}

		OnCountChanged(0,1);
		//set event listener
		if(system != null){
			system.OnCountChanged += OnCountChanged;
		}
	}

	void OnCountChanged(int p, int c){
		Txt_Count.Text = c.ToString() + "/" + system.Total.ToString();
	}
}
