﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_HUD : MonoBehaviour {

	public static G1_HUD Current;

	G1_GameManager manager;

	public DrawBitmapFont Txt_Score;
	public DrawBitmapFont Txt_HP;
	public DrawBitmapFont Txt_Lives;

	public Transform ItemSystemLocation;
	public G1_CollectibleItemDisplay ItemSystemDisplayPrefab;
	public List<G1_CollectibleSystem> CollectibleSystemList = new List<G1_CollectibleSystem>();

	G1_PlayerCharacter _char;
	public G1_PlayerCharacter PlayerRef {
		get{
			return _char;
		}
		set{ 
			if (_char != null) {
				_char.OnHealthChanged -= OnScoreChanged;
			}
			_char = value;
			_char.OnHealthChanged += OnHealthChanged;
		}
	}

	void Start(){
		Current = this;
		manager = G1_GameManager.Instance;

		if (PlayerRef == null) {
			PlayerRef = FindObjectOfType<G1_PlayerCharacter> ();
		}

		manager.OnScoreChanged += OnScoreChanged;
		manager.OnLivesChanged += OnLivesChanges;
		PlayerRef.OnHealthChanged += OnHealthChanged;

		OnHealthChanged (0, PlayerRef.HP);
		OnScoreChanged (0, manager.CurrentScore);
		OnLivesChanges (0, manager.PlayerLives);
	}

	public void OnLivesChanges(int prev, int current){
		Txt_Lives.Text = "x" + current.ToString ();
	}

	public void OnScoreChanged(int prev, int current){
		Txt_Score.Text = current.ToString ();
	}

	public void OnHealthChanged(int before, int after){
		Txt_HP.Text = "HP: " + after.ToString ();
	}

	public void CreateNewSystem(G1_CollectibleSystem system){
		Vector3 pos = Vector3.zero;
		foreach(var i in CollectibleSystemList){
			pos.y -= 64;
		}

		var newDisplay = Instantiate<G1_CollectibleItemDisplay>(ItemSystemDisplayPrefab);
		CollectibleSystemList.Add(system);
		newDisplay.transform.SetParent(ItemSystemLocation, false);
		newDisplay.transform.localPosition = pos;
		newDisplay.system = system;
	}
}
