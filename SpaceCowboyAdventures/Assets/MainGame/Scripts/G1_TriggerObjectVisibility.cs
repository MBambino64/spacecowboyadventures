﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_TriggerObjectVisibility : ButtonTrigger {

	public List<GameObject> ActivateOnPressed = new List<GameObject>();
	public List<GameObject> ActivateOnReleased = new List<GameObject>();
	public List<GameObject> DeactivateOnPressed = new List<GameObject>();
	public List<GameObject> DeactivateOnReleased = new List<GameObject>();
	
	public void Activate(){
		foreach(var g in ActivateOnPressed){
			g.SetActive(true);
		}
		foreach(var g in DeactivateOnPressed){
			g.SetActive(false);
		}
	}

	public void Deactivate(){
		foreach(var g in ActivateOnReleased){
			g.SetActive(true);
		}
		foreach(var g in DeactivateOnReleased){
			g.SetActive(false);
		}
	}

	public override void ButtonAction(GameObject source, G1_PressureButton.ButtonState state){
		if(state == G1_PressureButton.ButtonState.Pressed){
			Activate();
		}else if(state == G1_PressureButton.ButtonState.Released){
			Deactivate();
		}

	}
}
