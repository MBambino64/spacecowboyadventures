﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif
using GamewingResourceLibrary;

public interface IPlayerTrigger{
	void ActivateTrigger(GameObject source);
}

public class G1_PlayerCharacter : G1_CharacterBase {

	//other object refs
	public Camera cam;

	//public vars
	public float JumpPower = 8f;
	public float MoveSpeed = 4f;
	public float SwimSpeed = 2f;
	public float Acceleration = 20f;

	public GameObject DeathObject;

	//private vars
	Vector3 inputVelocity;
	float direction;

	bool isMoving = true;

	//animation vars
	Animator anim;
	float animMoveSpeed = 0;

	//input related
	SuperInput.ControllerType controller;
	SuperInput.ControllerNode inputMoveX;
	SuperInput.ControllerNode inputMoveY;
	SuperInput.ControllerNode inputJump;

	float VecToDir(Vector3 vec){
		float d = Vector3.SignedAngle(Vector3.forward, vec, Vector3.up);
		return 180 - d;
	}

	Vector3 DirToVec(float dir){
		Vector3 v = new Vector3(
			Mathf.Cos(Mathf.Deg2Rad * dir),
			0,
			Mathf.Sin(Mathf.Deg2Rad * dir)
		);
		return v;
	}

	// Use this for initialization
	void Start () {
		base.Start();
		//get component refs
		cam = Camera.main;
		anim = GetComponent<Animator>();

		//get input indexes
		controller = GlobalInput.Instance.controller;
		inputMoveX = controller.getByName("P1_Move_X");
		inputMoveY = controller.getByName("P1_Move_Y");
		inputJump = controller.getByName("Button1");
	}

	void Update(){
		if(controller.getPressed(4)){
			G1_PauseMenu.PauseGame();
		}

		base.Update ();

		isMoving = true;

		//get joystick input
		Vector2 joy = new Vector2 (inputMoveX.getValue(), inputMoveY.getValue());
		if (joy.magnitude > 1f) {
			if (Mathf.Abs (joy.x) > Mathf.Abs (joy.y)) {
				joy.y = (new Vector2(joy.x, joy.y * 1.1f)).normalized.y;
			} else {
				joy.x = (new Vector2(joy.x * 1.1f, joy.y)).normalized.x;
			}
		}
	
		if (joy.magnitude < 0.2f) {
			joy = Vector2.zero;
			isMoving = false;
		}

		//input
		if (isGrounded) {
			ApplyInputFriction(groundFriction);
			MoveWithInput(joy, MoveSpeed, Acceleration);

			RotateToDirection(Time.deltaTime * 360f);
		}else if(isUnderWater) {
			ApplyInputFriction(waterFriction);
			MoveWithInput(joy, SwimSpeed, Acceleration / 4f);

			RotateToDirection(Time.deltaTime * 180);
		} else {
			MoveWithInput(joy, MoveSpeed, Acceleration / 4f);

			RotateToDirection(Time.deltaTime * 45);
		}

		//move
		control.Move(inputVelocity * Time.deltaTime);
		SnapToGround ();

		//check for jump
		if (inputJump.getPressed () && isGrounded && !isUnderWater) {
			Jump(JumpPower);
		}
		//check for swim
		if(inputJump.getHeld() && isUnderWater){
			anim.SetBool("Swim", true);
			if(velocity.y < SwimSpeed){
				velocity.y += (GravityPower + JumpPower) * Time.deltaTime;
			}
		}else{
			anim.SetBool("Swim", false);
		}


		//update rotation
		if(isMoving){
			direction = VecToDir(new Vector3(joy.x, 0, joy.y));
			direction = direction + cam.transform.eulerAngles.y;
		}

		//send animationData
		anim.SetFloat("Speed", inputVelocity.magnitude / MoveSpeed);
		anim.SetBool("Ground", isGrounded);
		anim.SetBool("Fall", (velocity.y < 0 && !isGrounded));
	}
	
	public void ApplyInputFriction(float friction){
		inputVelocity = Vector3.MoveTowards (inputVelocity, Vector3.zero, Time.deltaTime * friction);
	}

	public void MoveWithInput(Vector2 joy, float speed, float acceleration){
		Vector3 camForward = Vector3.Scale(cam.transform.forward, new Vector3(1f,0f,1f)).normalized;
		Vector3 camRight = Vector3.Scale(cam.transform.right, new Vector3(1f,0f,1f)).normalized;
		if(joy.magnitude >= 0.25f){
			inputVelocity += (Vector3.Slerp (transform.forward, camForward, 1.0f) * -joy.y * speed) * Time.deltaTime * acceleration;
			inputVelocity += (Vector3.Slerp (transform.right, camRight, 1.0f) * joy.x * speed) * Time.deltaTime * acceleration;
			if (inputVelocity.magnitude > MoveSpeed * joy.magnitude) {
				inputVelocity = inputVelocity.normalized * speed * joy.magnitude;
			}
		}
	}

	protected void RotateToDirection(float delta){
		Vector3 currentAngle = transform.rotation.eulerAngles;
		if(isMoving){
			currentAngle.y = Mathf.MoveTowardsAngle(currentAngle.y, direction, delta);
			transform.eulerAngles = currentAngle;
		}
	}

	#region implemented abstract members of G1_CharacterBase

	public override void OnDeath (DamageType deathBy)
	{
		//create explosion
		GameObject.Instantiate(DeathObject, transform.position, Quaternion.identity);
		enabled = false;
        Respawn ();
    }

	protected override IEnumerator co_Respawn (float timer)
	{
		//yield return base.co_Respawn (timer);
		//cam.transform.position = transform.position;
		//reset
		//enabled = true;
		//_isDead = false;
		//OnHealthChanged (HP, 3);
		//HP = 3;
		velocity = Vector3.zero;
        //G1_GameManager.Instance.CurrentScore -= 1000;
		G1_GameManager.Instance.PlayerLives--;
		if (G1_GameManager.Instance.PlayerLives != 0) {
			G1_GameManager.Instance.LoadNewScene (SceneManager.GetActiveScene ().name, 1f);
		} else {
			Time.timeScale = 0.1f;
			G1_GameManager.Instance.LoadNewScene ("scn_GameOver", 2f);
		}
		yield return null;
    }
	#endregion

	public void Respawn(){
		GoToSpawn(1f);
	}

	public void Jump(float power){
		velocity.y = power;
		anim.SetTrigger("Jump");
	}

	void OnTriggerEnter(Collider col){
		//base.OnTriggerEnter(col);
		IPlayerTrigger t = col.gameObject.GetComponent(typeof(IPlayerTrigger)) as IPlayerTrigger;
		if(t != null){
			t.ActivateTrigger(gameObject);
		}
	}
}