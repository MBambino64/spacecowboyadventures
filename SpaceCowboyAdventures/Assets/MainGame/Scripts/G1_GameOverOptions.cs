﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary.MenuSystem;
using UnityEngine.SceneManagement;


public class G1_GameOverOptions : MonoBehaviour {

	class Menu_GameOverOptions : MenuItemList{

		void Start(){
			base.Start();

			InitMenu();
			Time.timeScale = 1f;

			G1_GameManager.Instance.SaveScoreToHighScoreList(new G1_GameManager.ScoreItem("Test", G1_GameManager.Instance.CurrentScore));

			foreach (var m in menuItemList){
				m.colorSelected = Color.yellow;
				m.colorDeselected = Color.gray;
				m.font.Align_H = DrawBitmapFont.Halign.Center;
			}
		}

		void InitMenu(){
			AddItem(MenuItem.Create("Restart Game", menu_RestartGame));
			AddItem(MenuItem.Create("Quit", menu_ReturnToHub));
		}

		void menu_RestartGame(MenuItem source, int val){
			G1_GameManager.Instance.InitGame();
			DisposeOfMenu ();
		}

		void menu_ReturnToHub(MenuItem source, int val){
			G1_GameManager.Instance.LoadNewScene ("Shell", 2f);
			DisposeOfMenu ();
		}

	}

	void Start(){
		var m = MenuItemList.Create<Menu_GameOverOptions>(transform.position + new Vector3(0,32,0));
		m.transform.parent = transform;
		m.transform.localScale = Vector3.one;
		m.transform.localPosition = new Vector3(0,-80,0);
	}

}
