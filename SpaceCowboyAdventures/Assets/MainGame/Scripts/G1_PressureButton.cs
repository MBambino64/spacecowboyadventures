﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ButtonTrigger : MonoBehaviour{
	abstract public void ButtonAction(GameObject source, G1_PressureButton.ButtonState state);
}

public class G1_PressureButton : MonoBehaviour{

	public enum ButtonState{NotHeld, Pressed, Held, Released};
	public enum ButtonType{Pressure, Hold, Toggle, Timed};

	//public
	public Transform buttonTop;
	public List<ButtonTrigger> Recievers = new List<ButtonTrigger>();

	public ButtonType Behavior = ButtonType.Pressure;
	public float PressTime = 0f;
	public bool SendPress = true;
	public bool SendRelease = true;
	public bool SendHeld = false;
	public bool SendNotHeld = false;

	//private
	Vector3 originalPos;
	[SerializeField]	
	Vector3 deltaPos;

	bool held = false;
	List<GameObject> pressers;

	// Use this for initialization
	void Start () {
		pressers = new List<GameObject>();
		if(buttonTop != null){
			originalPos = buttonTop.localPosition;
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach(var r in Recievers){
			if(SendHeld && held){
				Held();
			}
			if(SendNotHeld && !held){
				NotHeld();			
			}
		}
	}

	void Pressed(){
		if(!held){
			held = true;
			StartCoroutine(Co_PressButton(0.2f));
			foreach(var r in Recievers){
				r.ButtonAction(gameObject, ButtonState.Pressed);
			}
		}
	}

	void Released(){
		held = false;
		StartCoroutine(Co_ReleaseButton(0.2f));
		foreach(var r in Recievers){
			r.ButtonAction(gameObject, ButtonState.Released);
		}
	}

	void Held(){
		foreach(var r in Recievers){
			r.ButtonAction(gameObject, ButtonState.Held);
		}
	}

	void NotHeld(){
		foreach(var r in Recievers){
			r.ButtonAction(gameObject, ButtonState.NotHeld);
		}
	}

	void OnTriggerEnter(Collider col){
		pressers.Add(col.gameObject);
		Pressed();
	}

	void OnTriggerExit(Collider col){
		pressers.Remove(col.gameObject);
		if(pressers.Count == 0){
			Released();
		}
	}

	IEnumerator Co_PressButton(float time){
		float t = 0;
		while(t < time){
			buttonTop.localPosition = Vector3.Lerp(originalPos, deltaPos, t / time);
			t += Time.deltaTime;
			yield return null;
		}
		buttonTop.localPosition = deltaPos;
	}

	IEnumerator Co_ReleaseButton(float time){
		float t = 0;
		while(t < time){
			buttonTop.localPosition = Vector3.Lerp(originalPos, deltaPos, 1-(t / time));
			t += Time.deltaTime;
			yield return null;
		}
		buttonTop.localPosition = originalPos;
	}
}
