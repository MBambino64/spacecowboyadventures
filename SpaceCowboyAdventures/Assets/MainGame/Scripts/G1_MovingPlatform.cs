﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class G1_MovingPlatform : ButtonTrigger {

	public enum PlatformBehaviorType{
		AlwaysOn, PressToActivate, PressToDeactivate, HoldToMove, HoldToStop
	}

	Rigidbody r;
	public bool pressed = true;
	public PlatformBehaviorType PlatformBehaviour = PlatformBehaviorType.AlwaysOn;

	[SerializeField]
	List<Rigidbody> objectsToCarry = new List<Rigidbody>();

	[System.Serializable]
	public class WaypointItem{
		public Transform transform;
		public float Speed;
		public float WaitTime;
		public WaypointItem (Transform t, float spd, float wait = 0) {
			transform = t;
			Speed = spd;
			WaitTime = wait;
		}
	}

	public List<WaypointItem> Waypoints = new List<WaypointItem>();

	void Start(){
		r = GetComponent<Rigidbody>();
		if(PlatformBehaviour == PlatformBehaviorType.AlwaysOn){
			pressed = true;
		}
		if(Waypoints.Count < 0){
			pressed = false;
			Debug.Assert(false, "No waypoints!");
		}else{
			nextPos = Waypoints[index].transform.position;
		}
	}

	#region implemented abstract members of ButtonTrigger

	public override void ButtonAction (GameObject source, G1_PressureButton.ButtonState state) {
		if(state == G1_PressureButton.ButtonState.Pressed && PlatformBehaviour == PlatformBehaviorType.PressToActivate){
			pressed = true;
		}
		if(state == G1_PressureButton.ButtonState.Pressed && PlatformBehaviour == PlatformBehaviorType.PressToDeactivate){
			pressed = false;
		}
		if(PlatformBehaviour == PlatformBehaviorType.HoldToMove){
			if(state == G1_PressureButton.ButtonState.Held)
				pressed = true;
			else
				pressed = false;
		}
		if(PlatformBehaviour == PlatformBehaviorType.HoldToStop){
			if(state == G1_PressureButton.ButtonState.NotHeld)
				pressed = true;
			else
				pressed = false;
		}
	}

	#endregion


	int index = 0;
	float waitFor = 0;

	enum ModeType {Moving, Waiting}
	ModeType mode = ModeType.Moving;
	Vector3 nextPos;

	void FixedUpdate(){
		if(pressed == false){
			return;
		}

		if(mode == ModeType.Moving){
			if(Vector3.Distance(transform.position, nextPos) > Waypoints[index].Speed * Time.fixedDeltaTime){
				Vector3 delta = Vector3.MoveTowards(transform.position, nextPos, Waypoints[index].Speed * Time.fixedDeltaTime) - transform.position;
				r.MovePosition(transform.position + delta);
				foreach(Rigidbody rb in objectsToCarry){
					//rb.MovePosition(rb.transform.position + delta);
					rb.transform.position += delta;
				}
			}else{
				//find new target
				r.MovePosition(nextPos);
				mode = ModeType.Waiting;
				index = (index + 1) % Waypoints.Count;
				waitFor = Waypoints[index].WaitTime;
				nextPos = Waypoints[index].transform.position;
			}
		}else if(mode == ModeType.Waiting){
			if(waitFor > 0f){
				waitFor -= Time.fixedDeltaTime;
			}else{
				mode = ModeType.Moving;
			}
		}
	}

	void OnTriggerEnter(Collider col){

		Rigidbody rb = col.GetComponent<Rigidbody>();

		if(rb == null || col.GetType() == typeof(CharacterController)){
			return;
		}

		if(!objectsToCarry.Contains(rb)){
			print(rb);

			objectsToCarry.Add(rb);
		}

		/*
		CharacterController c = col.gameObject.GetComponent<CharacterController>();
		if(c != null){
			c.transform.SetParent(transform, true);
			//reset player size
			Rescale(c.transform, Vector3.one);
			//c.transform.localScale = Vector3.one;
		}
		*/
	}

	void OnTriggerExit(Collider col){
		Rigidbody rb = col.GetComponent<Rigidbody>();
		if(rb == null || col.GetType() == typeof(CharacterController)){
			return;
		}

		if(objectsToCarry.Remove(rb)){
			print("removed " + rb);
		}

		/*
		CharacterController c = col.gameObject.GetComponent<CharacterController>();
		if(c != null){
			c.transform.SetParent(null, true);
			c.transform.localScale = Vector3.one;
		}
		*/
	}

	void Rescale(Transform obj, Vector3 worldScale){
		obj.localScale = Vector3.one;
		Vector3 scale = new Vector3(
			worldScale.x / obj.lossyScale.x,
			worldScale.y / obj.lossyScale.y,
			worldScale.z / obj.lossyScale.z
		);
		obj.localScale = scale;
	}
}
