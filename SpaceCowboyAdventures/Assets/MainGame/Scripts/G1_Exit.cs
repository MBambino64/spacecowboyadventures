﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class G1_Exit : MonoBehaviour {

    public string nextLevel;

    void OnTriggerEnter()
    {
		G1_GameManager.Instance.LoadNewScene (nextLevel);
		Destroy (this);
    }
}
