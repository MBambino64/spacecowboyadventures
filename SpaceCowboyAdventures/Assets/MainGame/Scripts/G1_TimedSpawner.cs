﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_TimedSpawner : MonoBehaviour {

	public GameObject Spawn;
	public Vector3 SpawnVelocity;
	public float SecondsToNextSpawn = 2f;
	public bool DestroyOnTimer = true;
	public float SecondsToDestroy = 5f;

	float timer = 0;
	
	// Update is called once per frame
	void Update () {
		if (SecondsToNextSpawn <= 0.1f) {
			return;
		}

		timer += Time.deltaTime;
		while (timer >= SecondsToNextSpawn) {
			SpawnObject ();
			timer -= SecondsToNextSpawn;
		}
	}

	void SpawnObject(){
		GameObject g = Instantiate (Spawn, transform.position, Quaternion.identity);

		Rigidbody r = g.GetComponent<Rigidbody>();
		if(r != null){
			r.velocity = SpawnVelocity;
		}

		if (DestroyOnTimer) {
			Destroy (g, SecondsToDestroy);
		}
	}
}
