﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GamewingResourceLibrary;

public class G1_GameManager : MonoBehaviour {

	const int NUMBER_OF_HIGHSCORES = 10;

	[Serializable]
	public class ScoreItem{
		public string Name {get; set;}
		public int Score {get; set;}

		public ScoreItem () {
			Name = "MissingNo.";
			Score = 0;
		}

		public ScoreItem (string name, int score) {
			Name = name;
			Score = score;
		}
	}

	public delegate void ScoreChangedEvent(int previous, int current);
	public delegate void LivesChangedEvent(int previous, int current);

	public ScoreChangedEvent OnScoreChanged;
	public LivesChangedEvent OnLivesChanged;


	static G1_GameManager _instance = null;
	public static G1_GameManager Instance{
		get{
			if (_instance == null) {
				var m = new GameObject ("G1_GameManager");
				_instance = m.AddComponent<G1_GameManager> ();
			}

			return _instance;
		}
	}

	void Awake(){
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (this);
		}
	}

	Texture2D blackImage;
	float fadeAlpha = 0;
	void Start(){
		blackImage = new Texture2D (1, 1, TextureFormat.Alpha8, false);
		blackImage.filterMode = FilterMode.Point;
		blackImage.SetPixel (0, 0, Color.black);
		blackImage.Apply ();
	}

	void OnGUI (){
		if (fadeAlpha > 0) {
			GUI.color = new Color (1,1,1,fadeAlpha);
			GUI.DrawTexture (Screen.safeArea, blackImage);
			GUI.color = Color.white;
		}
	}

	//file info
	string saveFilePath;

	//scores
	int _score = 0;
	int _lives = 0;
	public int CurrentScore{
		get{ 
			return _score;
		}
		set{
			int newVal = Mathf.Max (value, 0);
			if(OnScoreChanged != null)
				OnScoreChanged (_score, newVal);
			_score = newVal;
		}
	}
	public int PlayerLives{
		get{
			return _lives;
		}
		set{
			int newVal = Mathf.Max (value, 0);
			if(OnLivesChanged != null)
				OnLivesChanged (_score, newVal);
			_lives = newVal;
		}
	}
	public List<ScoreItem> HighScoreList;

	bool isLoadingScene = false;
	public void LoadNewScene(string SceneName, float fadeTime = 1.5f){
		if (isLoadingScene == false) {
			StartCoroutine(Co_LoadNewScene (SceneName, fadeTime));
		}
	}

	IEnumerator Co_LoadNewScene(string Scene, float fadeTime){

		for (float i = 0; i < fadeTime; i += Time.unscaledDeltaTime) {
			//fade here
			fadeAlpha = (i / fadeTime);
			yield return null;
		}

		fadeAlpha = 1f;

		//load new scene
		yield return SceneManager.LoadSceneAsync (Scene, LoadSceneMode.Single);

		for (float i = 0; i < fadeTime; i += Time.unscaledDeltaTime) {
			//fade back here
			fadeAlpha = 1 - (i / fadeTime);
			yield return null;
		}

		fadeAlpha = 0f;
		isLoadingScene = false;
	}

	//level
	public int LevelNumber = 0;

	G1_GameManager () {
		saveFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HighScores.txt");
		CurrentScore = 0;
		HighScoreList = new List<ScoreItem>();

		LoadGame();
	}

	public void InitGame(){
		CurrentScore = 0;
		PlayerLives = 3;
		LevelNumber = 1;

		LoadNewScene ("level1");
	}

	public void SaveScoreToHighScoreList(ScoreItem score){
		HighScoreList.Add (score);
		HighScoreList.Sort ((x, y) => {
			if(x.Score > y.Score) return -1;
			if(x.Score < y.Score) return 1;
			return 0;
		});
		if (HighScoreList.Count > NUMBER_OF_HIGHSCORES) {
			HighScoreList = new List<ScoreItem> (HighScoreList.GetRange (0, NUMBER_OF_HIGHSCORES));
		}
		foreach(var s in HighScoreList){
			print(s.Score);
		}
		SaveGame ();
	}

	public void SaveGame(string path = null){
		if(path == null){
			path = saveFilePath;
		}

		//string json = JsonUtility.ToJson (HighScoreList);
		using (var file = new StreamWriter (path)) {
			foreach(var s in HighScoreList){
				file.WriteLine(s.Score);
				file.WriteLine(s.Name);
			}
		}
	}

	public bool LoadGame(string path = null){
		if(path == null){
			path = saveFilePath;
		}
			
		HighScoreList = new List<ScoreItem>();
		try{
			using(var file = new StreamReader(path)){
				while(!file.EndOfStream){
					ScoreItem readScore = new ScoreItem();
					string score = file.ReadLine();
					readScore.Score = int.Parse(score);
					readScore.Name = file.ReadLine();
					HighScoreList.Add(readScore);
				}
			}
			return true;
		}catch{
			return false;
		}
	}
}
