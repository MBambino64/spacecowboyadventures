﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_ShowHighScoreTable : MonoBehaviour {

	List<DrawBitmapFont> entries;

	// Use this for initialization
	void Start () {
		Vector3 pos = Vector3.down * 32;
		foreach(var s in G1_GameManager.Instance.HighScoreList){
			DrawBitmapFont name = DrawBitmapFont.Create(s.Name, 48);
			DrawBitmapFont score = DrawBitmapFont.Create(s.Score.ToString(), 48);

			name.Align_H = DrawBitmapFont.Halign.Right;
			name.transform.position = new Vector3(-48, pos.y);
			name.StringColor = Color.red;
			name.defaultColor = Color.red;

			score.Align_H = DrawBitmapFont.Halign.Left;
			score.transform.position = new Vector3(48, pos.y);
			score.StringColor = Color.green;
			score.defaultColor = Color.green;

			name.transform.SetParent(transform, false);
			score.transform.SetParent(transform, false);

			pos += Vector3.down * 48;
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
