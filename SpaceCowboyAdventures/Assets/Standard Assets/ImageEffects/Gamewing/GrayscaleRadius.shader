﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/GrayScaleRadius"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Ramp ("Grayscale Ramp", 2D) = "white" {}
		_start ("Effect Start", float) = 0
		_end ("Effect End", float) = 64
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _Ramp;
			sampler2D _CameraDepthTexture;
			float _start;
			float _end;
			float4 _Position;

			fixed4 frag (v2f i) : SV_Target
			{
				//fixed3 screenpos = ComputeScreenPos(_Position.xzyw);
				fixed3 screenpos = _Position.xyz;
				screenpos.z /= 100;
				float depth = Linear01Depth(tex2D(_CameraDepthTexture, i.uv));
				float disRadius = distance(screenpos, fixed3(i.uv.x, i.uv.y, depth));

				fixed4 col = tex2D(_MainTex, i.uv);
				float norm = clamp((disRadius-_start)/(_end-_start),0,1);
				float4 dis = tex2D(_Ramp, float2(norm,norm));
				//dis = norm;

				col.rgb = lerp(col.rgb, dot(col.rgb, float3(0.3,0.59,0.11)), dis.r);
				//col.rgb = lerp(col.rgb, dot(col.rgb, float3(0.3,0.59,0.11)), 0);


				//col.rgb = screenpos.z/1000;

				return col;
			}
			ENDCG
		}
	}
}
