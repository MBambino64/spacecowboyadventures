using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
	[ExecuteInEditMode]
	[AddComponentMenu("Image Effects/GrayScaleRadius")]
	public class GrayscaleRadius : ImageEffectBase {
		public Texture  textureRamp;

		public float Start = 0;
		public float End = 1;

		public Transform center;

		Camera thisCam;

		void Awake(){
			thisCam = GetComponent<Camera> ();
			//GetComponent<Camera> ().depthTextureMode |= DepthTextureMode.MotionVectors;
		}

		// Called by camera to apply image effect
		void OnRenderImage (RenderTexture source, RenderTexture destination) {
			material.SetTexture("_Ramp", textureRamp);
			material.SetFloat("_start", Start);
			material.SetFloat ("_end", End);

			if (center != null) {
				material.SetVector ("_Position", thisCam.WorldToViewportPoint (center.position));
			} else {
				material.SetVector ("_Position", new Vector4(0.5f,0.5f,0f,1f));	
			}
			Graphics.Blit (source, destination, material);
		}
	}
}