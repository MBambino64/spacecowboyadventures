using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
	[ExecuteInEditMode]
	[AddComponentMenu("Image Effects/GrayscaleRamp")]
	public class GrayscaleRamp : ImageEffectBase {
		public Texture  textureRamp;

		[Range(0f,1f)]
		public float Start = 0;
		[Range(0f,1f)]
		public float End = 1;

		void Awake(){
			//GetComponent<Camera> ().depthTextureMode |= DepthTextureMode.MotionVectors;
		}

		// Called by camera to apply image effect
		void OnRenderImage (RenderTexture source, RenderTexture destination) {
			material.SetTexture("_Ramp", textureRamp);
			material.SetFloat("_start", Start);
			material.SetFloat ("_end", End);

			Graphics.Blit (source, destination, material);
		}
	}
}
