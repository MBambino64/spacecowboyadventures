using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [ExecuteInEditMode]
    [RequireComponent (typeof(Camera))]
    [AddComponentMenu ("Image Effects/Displacement/ScreenDistort")]
    public class ScreenDistort : PostEffectsBase
	{
        [Range(0.0f, 4f)]
		public float intensity = 1f;
		public Texture2D NormalMap;

        public Shader fishEyeShader = null;
        private Material fisheyeMaterial = null;


        public override bool CheckResources ()
		{
            CheckSupport (false);
            fisheyeMaterial = CheckShaderAndCreateMaterial(fishEyeShader,fisheyeMaterial);

            if (!isSupported)
                ReportAutoDisable ();
            return isSupported;
        }

        void OnRenderImage (RenderTexture source, RenderTexture destination)
		{
            if (CheckResources()==false)
			{
                Graphics.Blit (source, destination);
                return;
            }

			fisheyeMaterial.SetFloat ("intensity", intensity);
			fisheyeMaterial.SetTexture ("_NormalTex", NormalMap);
            Graphics.Blit (source, destination, fisheyeMaterial);
        }
    }
}
